﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Porte_RFID
{
    /// <summary>
    /// Logique d'interaction pour Local.xaml
    /// </summary>
    public partial class Local : Window
    {
        public Local()
        {
            InitializeComponent();
        }

        private void local_click(object sender, RoutedEventArgs e)
        {
            Local wnd = new Local();

            wnd.Show();
        }
    }
}
